package com.cache;

import org.eclipse.jetty.websocket.api.Session;
import java.util.*;

public class UserCache {

    private final List<Session> users = new LinkedList<>();

    public List<Session> getCache() {
        return users;
    }

    public void addUser(Session session) {
        users.add(session);
    }

    public void closeSession(Session session) {
        users.remove(session);
    }
}
