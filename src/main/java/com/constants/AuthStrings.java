package com.constants;

public class AuthStrings {
    public static final String LOGIN = "login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    private AuthStrings() {
    }
}
