package com.constants;

public class DBProperties {
    public static final String PROPERTIES_HOST = "jdbc.url";
    public static final String PROPERTIES_USERNAME = "jdbc.username";
    public static final String PROPERTIES_PASSWORD = "jdbc.password";

    private DBProperties() {
    }
}
