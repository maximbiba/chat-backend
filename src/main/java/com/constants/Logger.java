package com.constants;

public class Logger {
    public static final String ACCESS_LOAD_PROPERTIES = "Properties was loaded";
    public static final String ACCESS_AUTH_USER = "User {} was authorized";
    public static final String START_SERVER = "Starting JETTY server";
    public static final String ACCESS_START_AT_HOST = "Configuration server started at: http://localhost:8080/auth";
    public static final String ACCESS_INITIALIZED_SERVLET = "Auth servlet initialized";
    public static final String AUTH_REQ_WITH_PARAM = "Authorization request with params: {}";
    public static final String EXCEPTION_NAME_MESSAGE = "Exception was thrown with name: {}, with message: {}";
    public static final String REG_SERV_INIT = "Register servlet initialized";
    public static final String REG_REQ_WITH_PARAM = "Registration request with params: {}";
    public static final String SOMETHING_WRONG = "Something goes wrong while parsing data";
    public static final String SUCCESS_CONNECTION = "Connection success";
    public static final String DISCONNECTED_SESSION = "disconnected";
    public static final String USER_DISCONNECTED = "User %s has disconnected to chat";
    public static final String USER_CONNECTED = "User %s has connected to chat";
    public static final String CONNECTED_SESSION = "connected";
    public static final String RECEIVED_TEXT = "received text {}";
    public static final String MSG = "Message: ";

    private Logger() {
    }
}
