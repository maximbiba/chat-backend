package com.constants;

public class MigrationScripts {

    public static final String INSERT_REGISTRATION_DATA = "INSERT INTO users (login, password, email, phone_number, company) VALUES (?, ?, ?, ?, ?);";
    public static final String CHECK_IF_USERS_AUTH_INFO_EXISTS = "SELECT * from USERS;";
    public static final String INSERT_NEW_MESSAGE_FOR_CHAT = "INSERT INTO messages (chat_name, data, message) VALUES (?, ?, ?);";
    public static final String CHECK_IF_EMAIL_EXISTS = "SELECT email from users;";
    public static final String UPDATE_PASSWORD_USING_EMAIL = "UPDATE users SET password = ? WHERE email = ?;";
    public static final String INSERT_DATA_FOR_NEW_CHAT = "INSERT INTO user_chat (user_id, chat_name) VALUES (?, ?);";

    private MigrationScripts() {
    }
}
