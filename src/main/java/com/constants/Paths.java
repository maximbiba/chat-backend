package com.constants;

public class Paths {

    public static final String DB_PROPERTIES_FILE = "application.properties";

    private Paths() {
    }
}
