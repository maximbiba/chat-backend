package com.constants;

public class Responses {

    public static final String POSTGRESQL_CONNECTION_ERROR = "Connection can't be established";
    public static final String DATABASE_PROPERTIES_FILE_ERROR = "Wrong database properties";
    public static final String WRONG_USER_DATA = "User not found";
    public static final String NEW_USER_SUCCESSFULLY_REGISTERED = "New user registered";

    private Responses() {
    }
}
