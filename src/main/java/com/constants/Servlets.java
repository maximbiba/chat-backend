package com.constants;

public class Servlets {

    public static final String CORS_FILTER_MAPPING = "/*";
    public static final String RESPONSE_CORS_HEADER = "Access-Control-Allow-Origin";
    public static final String CORS_RESPONSE_HEADER = "Access-Control-Allow-Methods";
    public static final String HTTP_REQUEST = "GET, OPTIONS, HEAD, PUT, POST";
    public static final String AUTH_HANDLER = "authHandler";
    public static final String REG_HANDLER = "regHandler";
    public static final String AUTH_MAPPING = "/auth";
    public static final String REG_MAPPING = "/reg";
    public static final String WRONG_PARS_DATA = "Something goes wrong while parsing data";
    public static final String CODER = "UTF-8";
    public static final String TEXT_PLAIN = "text/plain";

    private Servlets() {

    }
}
