package com.constants;

public class Validation {

        public static final String MSG_INVALID_NULL = "Fields cannot be null";
        public static final String MSG_INVALID_INCORRECT_PASSWORD = "Incorrect password";
        public static final String MSG_INVALID_INCORRECT_NUMBER = "Incorrect number";
        public static final String MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS = "Exceeded number of characters";
        public static final String REGEX_PASSWORD = "[A-Za-z0-9]{8,16}";
        public static final String REGEX_PASSWORD_2 = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,16})";
        public static final String REGEX_LOGIN = "^[a-zA-Z0-9](_(?!(\\.|_))|\\.(?!(_|\\.))|[a-zA-Z0-9]){1,25}[a-zA-Z0-9]$";
        public static final String REGEX_EMAIL = "^[a-zA-Z0-9+_\\.-]{1,32}+@[a-zA-Z0-9-]{1,32}\\.[a-zA-Z0-9-]{2,6}+$";
        public static final String REGEX_PHONE_NUMBER = "\\+[1-9]\\d{7,14}";
        public static final int MAX_SYMBOLS = 100;

        private Validation() {

        }
}
