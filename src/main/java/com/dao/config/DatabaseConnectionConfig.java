package com.dao.config;

import com.util.DriverManagerWrapper;
import java.sql.Connection;
import java.util.Properties;

import static com.constants.DBProperties.*;

public class DatabaseConnectionConfig {

    private final Properties properties;
    private final DriverManagerWrapper driverManagerWrapper;

    public DatabaseConnectionConfig(Properties properties, DriverManagerWrapper driverManagerWrapper) {
        this.properties = properties;
        this.driverManagerWrapper = driverManagerWrapper;
    }

    public synchronized Connection getConnection() {
        String url = properties.getProperty(PROPERTIES_HOST);
        String userName = properties.getProperty(PROPERTIES_USERNAME);
        String password = properties.getProperty(PROPERTIES_PASSWORD);
        return driverManagerWrapper.connection(url, userName, password);
    }
}
