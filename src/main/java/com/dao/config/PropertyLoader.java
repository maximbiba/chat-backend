package com.dao.config;

import com.constants.Paths;
import com.constants.Responses;
import com.exception.InternalServerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.constants.Logger.ACCESS_LOAD_PROPERTIES;
import static com.constants.Responses.DATABASE_PROPERTIES_FILE_ERROR;

public class PropertyLoader {
    private final Logger logger = LogManager.getLogger(PropertyLoader.class);

    public Properties loadProperties() {
        Properties properties = new Properties();
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(Paths.DB_PROPERTIES_FILE)) {
            properties.load(is);
            logger.info(ACCESS_LOAD_PROPERTIES);
            return properties;
        } catch (IOException e) {
            logger.error(DATABASE_PROPERTIES_FILE_ERROR);
            throw new InternalServerException(Responses.DATABASE_PROPERTIES_FILE_ERROR, HttpStatus.INTERNAL_SERVER_ERROR_500);
        }
    }
}
