package com.dao.queries;

import com.dao.config.DatabaseConnectionConfig;
import com.exception.BadRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static com.constants.AuthStrings.*;
import static com.constants.Logger.ACCESS_AUTH_USER;
import static com.constants.MigrationScripts.CHECK_IF_USERS_AUTH_INFO_EXISTS;
import static com.constants.Responses.WRONG_USER_DATA;

public class AuthorizeQuery {

    private final Logger logger = LogManager.getLogger(AuthorizeQuery.class);
    private final DatabaseConnectionConfig databaseConnectionConfig;

    public AuthorizeQuery(DatabaseConnectionConfig databaseConnectionConfig) {
        this.databaseConnectionConfig = databaseConnectionConfig;
    }

    public String authorizeUser(String userLoginOrEmail, String userPassword) {
        try (Connection connection = databaseConnectionConfig.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(CHECK_IF_USERS_AUTH_INFO_EXISTS);
            while (rs.next()) {
                String login = rs.getString(LOGIN);
                String email = rs.getString(EMAIL);
                String pass = rs.getString(PASSWORD);
                if ((login.equals(userLoginOrEmail) || email.equals(userLoginOrEmail)) && pass.equals(userPassword)) {
                    logger.info(ACCESS_AUTH_USER, userLoginOrEmail);
                    return login;
                }
            }
        } catch (SQLException e) {
            logger.warn(WRONG_USER_DATA);
        }

        throw new BadRequestException(WRONG_USER_DATA, 400);
    }
}
