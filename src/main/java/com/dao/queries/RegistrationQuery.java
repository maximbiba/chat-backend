package com.dao.queries;

import com.dao.config.DatabaseConnectionConfig;
import com.dto.UserRegDto;
import com.exception.BadRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.*;
import static com.constants.MigrationScripts.CHECK_IF_USERS_AUTH_INFO_EXISTS;
import static com.constants.MigrationScripts.INSERT_REGISTRATION_DATA;
import static com.constants.Responses.NEW_USER_SUCCESSFULLY_REGISTERED;
import static com.constants.Responses.WRONG_USER_DATA;

public class RegistrationQuery {

    private final Logger logger = LogManager.getLogger(RegistrationQuery.class);
    private final DatabaseConnectionConfig databaseConnectionConfig;

    public RegistrationQuery(DatabaseConnectionConfig databaseConnectionConfig) {
        this.databaseConnectionConfig = databaseConnectionConfig;
    }

    public boolean checkIfExist(String verifiedLogin, String verifiedEmail) {
        try (Connection connection = databaseConnectionConfig.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(CHECK_IF_USERS_AUTH_INFO_EXISTS);
            while (rs.next()) {
                String login = rs.getString("login");
                String email = rs.getString("email");
                if (login.equals(verifiedLogin) || email.equals(verifiedEmail)) {
                    logger.info("Login {} or email {} aready exists", login, email);
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            logger.warn(WRONG_USER_DATA);
            throw new BadRequestException(WRONG_USER_DATA, 400);
        }
    }

    public void registerUser(UserRegDto user) {
        try {
            Connection connection = databaseConnectionConfig.getConnection();
            PreparedStatement preparedStatement;
            preparedStatement = connection.prepareStatement(INSERT_REGISTRATION_DATA);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPhoneNumber());
            preparedStatement.setString(5, user.getCompany());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new BadRequestException(e.getMessage(), 400);
        }

        logger.info(NEW_USER_SUCCESSFULLY_REGISTERED);
    }
}
