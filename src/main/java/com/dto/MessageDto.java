package com.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;

public class MessageDto implements Serializable {
    private final String message;
    private final String login;
    private final String chatName;
    private final long date;

    public MessageDto(@JsonProperty("message") String message,
                      @JsonProperty("login") String login,
                      @JsonProperty("chatName") String chatName,
                      @JsonProperty("date") long date) {
        this.message = message;
        this.login = login;
        this.chatName = chatName;
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public String getLogin() {
        return login;
    }

    public String getChatName() {
        return chatName;
    }

    public long getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "MessageDto{" +
                "message='" + message + '\'' +
                ", login='" + login + '\'' +
                ", chatName='" + chatName + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDto that = (MessageDto) o;
        return date == that.date && message.equals(that.message) && login.equals(that.login) && chatName.equals(that.chatName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, login, chatName, date);
    }
}
