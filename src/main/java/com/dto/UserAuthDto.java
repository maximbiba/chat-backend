package com.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class UserAuthDto {

    private final String loginOrEmail;
    private final String password;

    public UserAuthDto(@JsonProperty("loginOrEmail") String loginOrEmail, @JsonProperty("password") String password) {
        this.loginOrEmail = loginOrEmail;
        this.password = password;
    }

    public String getLoginOrEmail() {
        return loginOrEmail;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "UserAuthDto{" +
                "loginOrEmail='" + loginOrEmail + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserAuthDto that = (UserAuthDto) o;
        return loginOrEmail.equals(that.loginOrEmail) && password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loginOrEmail, password);
    }
}
