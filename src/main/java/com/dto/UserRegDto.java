package com.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

public class UserRegDto {

    private final String login;
    private final String email;
    private String password;
    private final String phoneNumber;
    private final String company;

    public UserRegDto(@JsonProperty("login") String login,
                      @JsonProperty("email") String email,
                      @JsonProperty("password") String password,
                      @JsonProperty("phoneNumber") String phoneNumber,
                      @JsonProperty("company") String company) {

        this.email = email;
        this.login = login;
        this.company = company;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getCompany() {
        return company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserRegDto that = (UserRegDto) o;
        return email.equals(that.email) && login.equals(that.login) && company.equals(that.company) && phoneNumber.equals(that.phoneNumber) && password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, login, company, phoneNumber, password);
    }

    @Override
    public String toString() {
        return "UserRegDto{" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
