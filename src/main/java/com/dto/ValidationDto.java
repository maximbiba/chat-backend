package com.dto;

public class ValidationDto {

    private final String message;
    private final boolean isValid;

    public ValidationDto(String message, boolean isValid) {
        this.message = message;
        this.isValid = isValid;
    }

    public boolean isValid() {
        return isValid;
    }

    public String getMessage() {
        return message;
    }
}
