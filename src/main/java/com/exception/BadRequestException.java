package com.exception;

public class BadRequestException extends GlobalException {
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, int statusCode) {
        super(message, statusCode);
    }
}
