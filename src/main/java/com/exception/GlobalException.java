package com.exception;

public class GlobalException extends RuntimeException {
    private int statusCode;

    public GlobalException(String message) {
        super(message);
    }

    public GlobalException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
