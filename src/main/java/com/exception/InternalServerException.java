package com.exception;

public class InternalServerException extends GlobalException {
    public InternalServerException(String message) {
        super(message);
    }

    public InternalServerException(String message, int statusCode) {
        super(message, statusCode);
    }
}
