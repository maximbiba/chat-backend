package com.exception;

public class UnauthorizedException extends GlobalException {
    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(String message, int statusCode) {
        super(message, statusCode);
    }
}
