package com.server;

import com.servlets.handler.ServletsHandlers;
import com.websockethandler.WebSocketHandlerProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;

import static com.constants.Logger.ACCESS_START_AT_HOST;
import static com.constants.Logger.START_SERVER;

public class ServerConnection {

    private static final Logger logger = LogManager.getLogger(ServerConnection.class);

    public static void main(String ...args) throws Exception {
        Server server = new Server(8080);
        ServletsHandlers servletsHandlers = new ServletsHandlers();
        HandlerList handlerList = new HandlerList();

        logger.info(START_SERVER);
        server.setHandler(handlerList);
        handlerList.addHandler(new WebSocketHandlerProvider());
        handlerList.addHandler(servletsHandlers.getContextHandlerCollection());
        server.start();
        logger.info(ACCESS_START_AT_HOST);
        server.join();
    }
}
