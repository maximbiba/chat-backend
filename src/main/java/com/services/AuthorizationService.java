package com.services;

import com.dao.queries.AuthorizeQuery;
import com.dto.UserAuthDto;
import com.dto.ValidationDto;
import com.exception.BadRequestException;
import com.util.PasswordHashHelper;
import com.validators.UserAuthValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AuthorizationService {

    private final Logger logger = LogManager.getLogger(AuthorizationService.class);
    private final AuthorizeQuery authorizeQuery;
    private final PasswordHashHelper passwordHashHelper;
    private final UserAuthValidator validator;

    public AuthorizationService(AuthorizeQuery authorizeQuery, PasswordHashHelper passwordHashHelper, UserAuthValidator validator) {
        this.authorizeQuery = authorizeQuery;
        this.passwordHashHelper = passwordHashHelper;
        this.validator = validator;
    }

    public String authUser(UserAuthDto userAuthDto) {
        ValidationDto validationDto = validator.validateUserAuth(userAuthDto);
        if (!validationDto.isValid()) {
            logger.warn("Validation failed with message {} on model {}", validationDto.getMessage(), userAuthDto);
            throw new BadRequestException(validationDto.getMessage(), 400);
        }

        String hashPassword = passwordHashHelper.hashPassword(userAuthDto.getPassword());
        return authorizeQuery.authorizeUser(userAuthDto.getLoginOrEmail(), hashPassword);
    }
}

