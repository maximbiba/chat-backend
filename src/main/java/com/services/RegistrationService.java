package com.services;

import com.dao.queries.RegistrationQuery;
import com.dto.UserRegDto;
import com.dto.ValidationDto;
import com.exception.BadRequestException;
import com.util.PasswordHashHelper;
import com.validators.UserRegValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RegistrationService {

    private final Logger logger = LogManager.getLogger(RegistrationService.class);
    private final RegistrationQuery registrationQuery;
    private final PasswordHashHelper passwordHashHelper;
    private final UserRegValidator userRegValidator;

    public RegistrationService(RegistrationQuery registrationQuery, PasswordHashHelper passwordHashHelper, UserRegValidator userRegValidator) {
        this.registrationQuery = registrationQuery;
        this.passwordHashHelper = passwordHashHelper;
        this.userRegValidator = userRegValidator;
    }

    public void registerUser(UserRegDto userRegDto) {
        ValidationDto validationDto = userRegValidator.validateUserRegDto(userRegDto);
        if (!validationDto.isValid()) {
            logger.warn("Validation failed with message {} on model {}", validationDto.getMessage(), userRegDto);
            throw new BadRequestException(validationDto.getMessage(), 400);
        }

        if (registrationQuery.checkIfExist(userRegDto.getLogin(), userRegDto.getEmail())) {
            logger.warn("User already exists {}", userRegDto);
            throw new BadRequestException(String.format("User already exists %s", userRegDto), 400);
        }

        String hashPassword = passwordHashHelper.hashPassword(userRegDto.getPassword());
        userRegDto.setPassword(hashPassword);
        registrationQuery.registerUser(userRegDto);
    }
}
