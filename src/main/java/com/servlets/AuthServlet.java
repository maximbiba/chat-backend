package com.servlets;

import com.dto.UserAuthDto;
import com.exception.GlobalException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.AuthorizationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpHeader;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;
import static com.constants.Logger.*;
import static com.constants.Servlets.*;

public class AuthServlet extends HttpServlet {

    private final Logger logger = LogManager.getLogger(AuthServlet.class);
    private final ObjectMapper objectMapper;
    private final AuthorizationService authorizationService;

    public AuthServlet(ObjectMapper objectMapper, AuthorizationService authorizationService) {
        this.objectMapper = objectMapper;
        this.authorizationService = authorizationService;
    }

    @Override
    public void init(ServletConfig config) {
        logger.info(ACCESS_INITIALIZED_SERVLET);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            resp.setHeader(HttpHeader.CONTENT_TYPE.toString(), TEXT_PLAIN);
            resp.setCharacterEncoding(CODER);
            String requestData = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            UserAuthDto userAuthDto = objectMapper.readValue(requestData, UserAuthDto.class);
            logger.info(AUTH_REQ_WITH_PARAM, userAuthDto);
            String login = authorizationService.authUser(userAuthDto);
            resp.getWriter().write(login);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (IOException e) {
            logger.warn(EXCEPTION_NAME_MESSAGE, e.getClass().getSimpleName(), e.getMessage());
            resp.getWriter().write(WRONG_PARS_DATA);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } catch (GlobalException e) {
            logger.warn(EXCEPTION_NAME_MESSAGE, e.getClass().getSimpleName(), e.getMessage());
            resp.getWriter().write(e.getMessage());
            resp.setStatus(e.getStatusCode());
        }
    }
}
