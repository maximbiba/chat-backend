package com.servlets;

import com.dto.UserRegDto;
import com.exception.GlobalException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.RegistrationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpHeader;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;
import static com.constants.Logger.*;
import static com.constants.Servlets.CODER;
import static com.constants.Servlets.TEXT_PLAIN;

public class RegistrationServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(RegistrationServlet.class);
    private final ObjectMapper objectMapper;
    private final RegistrationService registrationService;

    public RegistrationServlet(ObjectMapper objectMapper, RegistrationService registrationService) {
        this.registrationService = registrationService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void init(ServletConfig config) {
        logger.info(REG_SERV_INIT);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            resp.setHeader(HttpHeader.CONTENT_TYPE.toString(), TEXT_PLAIN);
            resp.setCharacterEncoding(CODER);
            String requestData = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            UserRegDto user = objectMapper.readValue(requestData, UserRegDto.class);
            logger.info(REG_REQ_WITH_PARAM, user);
            registrationService.registerUser(user);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (IOException e) {
            logger.warn(EXCEPTION_NAME_MESSAGE, e.getClass().getSimpleName(), e.getMessage());
            resp.getWriter().write(SOMETHING_WRONG);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        } catch (GlobalException e) {
            logger.warn(EXCEPTION_NAME_MESSAGE, e.getClass().getSimpleName(), e.getMessage());
            resp.getWriter().write(e.getMessage());
            resp.setStatus(e.getStatusCode());
        }
    }
}



