package com.servlets.handler;

import com.dao.config.DatabaseConnectionConfig;
import com.dao.config.PropertyLoader;
import com.dao.queries.AuthorizeQuery;
import com.dao.queries.RegistrationQuery;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.AuthorizationService;
import com.services.RegistrationService;
import com.servlets.AuthServlet;
import com.servlets.RegistrationServlet;
import com.servlets.filter.CORSFilter;
import com.util.DriverManagerWrapper;
import com.util.PasswordHashHelper;
import com.validators.UserAuthValidator;
import com.validators.UserRegValidator;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import java.util.EnumSet;
import java.util.Properties;

import static com.constants.Servlets.*;

public class ServletsHandlers {

    public ContextHandlerCollection getContextHandlerCollection () {
        PropertyLoader propertyLoader = new PropertyLoader();
        Properties properties = propertyLoader.loadProperties();

        DriverManagerWrapper driverManagerWrapper = new DriverManagerWrapper();
        DatabaseConnectionConfig databaseConnectionConfig = new DatabaseConnectionConfig(properties, driverManagerWrapper);

        AuthorizeQuery authorizeQuery = new AuthorizeQuery(databaseConnectionConfig);
        RegistrationQuery registrationQuery = new RegistrationQuery(databaseConnectionConfig);

        PasswordHashHelper passwordHashHelper = new PasswordHashHelper();
        UserAuthValidator userAuthValidator = new UserAuthValidator();
        UserRegValidator userRegValidator = new UserRegValidator();

        AuthorizationService authorizationService = new AuthorizationService(authorizeQuery, passwordHashHelper, userAuthValidator);
        RegistrationService registrationService = new RegistrationService(registrationQuery, passwordHashHelper, userRegValidator);

        ObjectMapper objectMapper = new ObjectMapper();

        ServletHolder authHandler = new ServletHolder(AUTH_HANDLER, new AuthServlet(objectMapper, authorizationService));
        ServletHolder regHandler = new ServletHolder(REG_HANDLER, new RegistrationServlet(objectMapper, registrationService));

        ServletHandler servletHandler = new ServletHandler();
        servletHandler.setServlets(new ServletHolder[]{authHandler, regHandler});

        ServletContextHandler servletContextHandler = new ServletContextHandler();
        servletContextHandler.addServlet(authHandler, AUTH_MAPPING);
        servletContextHandler.addServlet(regHandler, REG_MAPPING);
        servletContextHandler.addFilter(CORSFilter.class, CORS_FILTER_MAPPING, EnumSet.of(DispatcherType.REQUEST));

        ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();
        contextHandlerCollection.setHandlers(new Handler[]{servletContextHandler});
        return contextHandlerCollection;
    }
}
