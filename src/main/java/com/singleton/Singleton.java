package com.singleton;

import com.cache.UserCache;

public class Singleton {
    public static final UserCache USER_CACHE = new UserCache();

    private Singleton() {
    }
}
