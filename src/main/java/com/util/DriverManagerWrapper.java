package com.util;

import com.constants.Responses;
import com.exception.InternalServerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.http.HttpStatus;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.constants.Logger.SUCCESS_CONNECTION;

public class DriverManagerWrapper {

    private final Logger logger = LogManager.getLogger(DriverManagerWrapper.class);

    public Connection connection(String url, String userName, String password) {
        try {
            Connection connection = DriverManager.getConnection(url, userName, password);
            logger.info(SUCCESS_CONNECTION);
            return connection;
        } catch (SQLException e) {
            logger.error(Responses.POSTGRESQL_CONNECTION_ERROR);
            throw new InternalServerException(Responses.POSTGRESQL_CONNECTION_ERROR, HttpStatus.INTERNAL_SERVER_ERROR_500);
        }
    }
}
