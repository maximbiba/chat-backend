package com.util;


import org.apache.commons.codec.digest.DigestUtils;

public class PasswordHashHelper {
    public String hashPassword(String password) {
        return DigestUtils.sha512Hex(password);
    }

}
