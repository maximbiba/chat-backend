package com.validators;

import com.dto.UserAuthDto;
import com.dto.ValidationDto;

public class UserAuthValidator {

    public ValidationDto validateUserAuth(UserAuthDto userAuthDto) {
        return checkFieldsToNull(userAuthDto) && checkFieldsToEmpty(userAuthDto)
                ? new ValidationDto(null, true)
                : new ValidationDto("Some fields are empty", false);
    }

    private boolean checkFieldsToNull(UserAuthDto userAuthDto) {
        return userAuthDto.getLoginOrEmail() != null && userAuthDto.getPassword() != null;
    }

    private boolean checkFieldsToEmpty(UserAuthDto userAuthDto) {
        return !userAuthDto.getLoginOrEmail().isEmpty() && !userAuthDto.getPassword().isEmpty();
    }
}