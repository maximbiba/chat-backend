package com.validators;

import com.dto.UserRegDto;
import com.dto.ValidationDto;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static com.constants.Validation.*;

public class UserRegValidator {

    public ValidationDto validateUserRegDto(UserRegDto userRegDto) {
        if (!validateOnNullFields(userRegDto)) {
            return new ValidationDto(MSG_INVALID_NULL, false);
        }

        if (!validatePassword(userRegDto.getPassword())) {
            return new ValidationDto(MSG_INVALID_INCORRECT_PASSWORD, false);
        }

        if (!validateLogin(userRegDto.getLogin())) {
            return new ValidationDto("Incorrect login", false);
        }

        if (!validateEmail(userRegDto.getEmail())) {
            return new ValidationDto("Incorrect email", false);
        }

        if (!validatePhoneNumber(userRegDto.getPhoneNumber())) {
            return new ValidationDto(MSG_INVALID_INCORRECT_NUMBER, false);
        }

        if (!validateCompanyName(userRegDto.getCompany())) {
            return new ValidationDto(MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS, false);
        }

        return new ValidationDto(null, true);
    }

    private boolean validateOnNullFields(UserRegDto userRegDto) {
        return userRegDto != null && userRegDto.getEmail() != null
                && userRegDto.getLogin() != null && userRegDto.getPassword() != null
                && userRegDto.getPhoneNumber() != null;
    }

    private boolean validatePassword(String password) {
        return checkWithRegexp(password, REGEX_PASSWORD) || checkWithRegexp(password, REGEX_PASSWORD_2);
    }

    private boolean validateLogin(String login) {
        return checkWithRegexp(login, REGEX_LOGIN) && login.length() <= MAX_SYMBOLS;
    }

    private boolean validateEmail(String email) {
        return checkWithRegexp(email, REGEX_EMAIL);
    }

    private boolean validatePhoneNumber(String phoneNumber) {
        return checkWithRegexp(phoneNumber, REGEX_PHONE_NUMBER);
    }

    private boolean validateCompanyName(String companyName) {
        return companyName.length() <= MAX_SYMBOLS;
    }

    private boolean checkWithRegexp(String field, String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(field);
        return matcher.matches();
    }
}
