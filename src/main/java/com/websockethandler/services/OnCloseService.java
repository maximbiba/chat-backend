package com.websockethandler.services;

import com.cache.UserCache;
import com.dto.MessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import java.io.IOException;
import java.net.HttpCookie;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import static com.constants.Logger.DISCONNECTED_SESSION;
import static com.constants.Logger.USER_DISCONNECTED;

public class OnCloseService {
    private final Logger logger = LogManager.getLogger(OnCloseService.class);
    private final UserCache userCache;
    private final ObjectMapper objectMapper;
    public OnCloseService(ObjectMapper objectMapper, UserCache userCache) {
        this.userCache = userCache;
        this.objectMapper = objectMapper;
    }

    public void onClose(Session session) {
        logger.info(DISCONNECTED_SESSION);
        try {
            userCache.closeSession(session);
            List<Session> sessions = userCache.getCache();
            for (Session userSession : sessions) {
                if (session != null && !session.equals(userSession)) {
                    userSession.getRemote().sendString(objectMapper.writeValueAsString(new MessageDto(String.format(USER_DISCONNECTED, getLoginFromCookie(session)),
                            "Server", "General", Instant.now().toEpochMilli())));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private String getLoginFromCookie(Session session) {
        String login = "";
        List<HttpCookie> httpCookieList = session.getUpgradeRequest().getCookies();

        for (HttpCookie cookie : httpCookieList) {
            if (cookie.getName().equals("login")) {
                login = cookie.getValue();
                break;
            }
        }
        return login;
    }
}
