package com.websockethandler.services;

import com.cache.UserCache;
import com.dto.MessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import java.io.IOException;
import java.net.HttpCookie;
import java.time.Instant;
import java.util.List;
import static com.constants.Logger.*;

public class OnOpenService {
    private final Logger logger = LogManager.getLogger(OnOpenService.class);
    private final UserCache userCache;
    private final ObjectMapper objectMapper;

    public OnOpenService(ObjectMapper objectMapper, UserCache userCache) {
        this.userCache = userCache;
        this.objectMapper = objectMapper;
    }

    public void onOpen(Session session) {
        logger.info(CONNECTED_SESSION);
        try {
            userCache.addUser(session);
            List<Session> sessions = userCache.getCache();
            for (Session userSession : sessions) {
                if (session != null && !session.equals(userSession)) {
                    MessageDto messageDto = new MessageDto(String.format(USER_CONNECTED, getLoginFromCookie(session)), "Server", "General", Instant.now().toEpochMilli());
                    userSession.getRemote().sendString(objectMapper.writeValueAsString(messageDto));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private String getLoginFromCookie(Session session) {
        String login = "";
        List<HttpCookie> httpCookieList = session.getUpgradeRequest().getCookies();

        for (HttpCookie cookie : httpCookieList) {
            if (cookie.getName().equals("login")) {
                login = cookie.getValue();
                break;
            }
        }
        return login;
    }
}
