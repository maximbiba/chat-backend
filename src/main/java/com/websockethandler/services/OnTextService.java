package com.websockethandler.services;

import com.cache.UserCache;
import com.dto.MessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import java.io.IOException;
import java.util.List;
import static com.constants.Logger.MSG;
import static com.constants.Logger.RECEIVED_TEXT;

public class OnTextService {
    private final Logger logger = LogManager.getLogger(OnTextService.class);
    private final UserCache userCache;

    public OnTextService(UserCache userCache) {
        this.userCache = userCache;
    }

    public void onText(Session session, String message) {
        logger.info(RECEIVED_TEXT, message);
        try {
            List<Session> sessions = userCache.getCache();
            for (Session usersSession : sessions) {
                if (session != null && !session.equals(usersSession)) {
                    usersSession.getRemote().sendString(message);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
