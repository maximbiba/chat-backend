package com.websockets;

import com.cache.UserCache;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.singleton.Singleton;
import com.websockethandler.services.OnCloseService;
import com.websockethandler.services.OnOpenService;
import com.websockethandler.services.OnTextService;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket(maxTextMessageSize = Integer.MAX_VALUE)
public class ChatEndPoint {
    private final OnTextService onTextService;
    private final OnCloseService onCloseService;
    private final OnOpenService onOpenService;

    public ChatEndPoint() {
        ObjectMapper objectMapper = new ObjectMapper();
        UserCache userCache = Singleton.USER_CACHE;
        onTextService = new OnTextService(userCache);
        onCloseService = new OnCloseService(objectMapper, userCache);
        onOpenService = new OnOpenService(objectMapper, userCache);
    }

    @OnWebSocketMessage
    public void onText(Session session, String message) {
        onTextService.onText(session, message);
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        onOpenService.onOpen(session);
    }

    @OnWebSocketClose
    public void onClose(Session session, int status, String reason) {
        onCloseService.onClose(session);
    }
}
