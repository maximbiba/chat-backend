CREATE SEQUENCE IF NOT EXISTS user_id_seq;
CREATE SEQUENCE IF NOT EXISTS user_chat_seq;

CREATE TABLE IF NOT EXISTS users
(
    id           int8        NOT NULL PRIMARY KEY,
    login        VARCHAR(30) NOT NULL,
    password     VARCHAR(512) NOT NULL,
    email        VARCHAR(30) NOT NULL,
    phone_number VARCHAR(15) NOT NULL,
    company      VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS user_chat
(
    id        int8        NOT NULL PRIMARY KEY,
    user_id   int8        NOT NULL REFERENCES users (id),
    chat_name VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS messages
(
    user_chat_id int8         NOT NULL REFERENCES user_chat (id),
    date         timestamp    NOT NULL,
    message      VARCHAR(300) NOT NULL
);


ALTER SEQUENCE user_id_seq OWNED BY users.id;
ALTER SEQUENCE user_chat_seq OWNED BY user_chat.id;
ALTER TABLE users ALTER COLUMN id SET DEFAULT nextval('user_id_seq');
ALTER TABLE user_chat ALTER COLUMN id SET DEFAULT nextval('user_chat_seq');

--test data
INSERT INTO users (login, password, email, phone_number, company)
VALUES ('Bill', '1q2w3e4r5t', 'bill@gmail.com', '0501111111', 'Alpha'),
       ('Ron', '1qaz2wsx', 'ron@gmail.com', '0502222222', 'Beta'),
       ('Mike', '3edc4rfv', 'mike@gmail.com', '0503333333', 'Gamma'),
       ('Anna', '5tgb6yhn', 'anna@gmail.com', '0504444444', 'Delta');

INSERT INTO user_chat (user_id, chat_name)
VALUES (1, 'General'),
       (2, 'General'),
       (3, 'General'),
       (4, 'General'),
       (1, 'Chat1'),
       (2, 'Chat1');

INSERT INTO messages (user_chat_id, date, message)
VALUES (1, '2022-01-27 19:30:06', 'Hello!'),
       (2, '2022-01-27 15:30:45', 'goodbye Peter!'),
       (5, '2022-01-27 19:31:06', 'Guten tag!'),
       (6, '2022-01-27 14:30:55', 'asdfg')
