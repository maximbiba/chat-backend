package com.dao.queries;

import com.dao.config.DatabaseConnectionConfig;
import com.exception.BadRequestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static com.constants.AuthStrings.*;
import static com.constants.MigrationScripts.CHECK_IF_USERS_AUTH_INFO_EXISTS;
import static com.constants.Responses.WRONG_USER_DATA;

class AuthorizeQueryTest {

    private final DatabaseConnectionConfig databaseConnectionConfig = Mockito.mock(DatabaseConnectionConfig.class);
    private final AuthorizeQuery cut = new AuthorizeQuery(databaseConnectionConfig);

    @Test
    void authorizeUserTest() throws SQLException {
        Connection connection = Mockito.mock(Connection.class);
        Statement statement = Mockito.mock(Statement.class);
        ResultSet resultSet = Mockito.mock(ResultSet.class);

        Mockito.when(databaseConnectionConfig.getConnection()).thenReturn(connection);
        Mockito.when(connection.createStatement()).thenReturn(statement);
        Mockito.when(statement.executeQuery(CHECK_IF_USERS_AUTH_INFO_EXISTS)).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(LOGIN)).thenReturn("koshka");
        Mockito.when(resultSet.getString(EMAIL)).thenReturn("koshka@gmail.com");
        Mockito.when(resultSet.getString(PASSWORD)).thenReturn("qwertY123");

        Assertions.assertDoesNotThrow(() -> cut.authorizeUser("koshka", "qwertY123"));
    }

    @Test
    void authorizeUserBadRequestTest() throws SQLException {
        Connection connection = Mockito.mock(Connection.class);
        Statement statement = Mockito.mock(Statement.class);

        Mockito.when(databaseConnectionConfig.getConnection()).thenReturn(connection);
        Mockito.when(connection.createStatement()).thenReturn(statement);
        Mockito.when(statement.executeQuery(CHECK_IF_USERS_AUTH_INFO_EXISTS)).thenThrow(SQLException.class);

        BadRequestException badRequestException = Assertions.assertThrows(BadRequestException.class, () -> cut.authorizeUser("koshka", "qwertY123"));

        Assertions.assertEquals(WRONG_USER_DATA, badRequestException.getMessage());
        Assertions.assertEquals(400, badRequestException.getStatusCode());
    }
}
