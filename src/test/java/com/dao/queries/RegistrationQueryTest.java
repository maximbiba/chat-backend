package com.dao.queries;

import com.dao.config.DatabaseConnectionConfig;
import com.dto.UserRegDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.*;

import static com.constants.AuthStrings.*;
import static com.constants.MigrationScripts.CHECK_IF_USERS_AUTH_INFO_EXISTS;
import static com.constants.MigrationScripts.INSERT_REGISTRATION_DATA;

public class RegistrationQueryTest {
    private final DatabaseConnectionConfig databaseConnectionConfig = Mockito.mock(DatabaseConnectionConfig.class);
    private final RegistrationQuery cut = new RegistrationQuery(databaseConnectionConfig);

    @Test
    void registerUserTest() throws SQLException {
        UserRegDto user = new UserRegDto("Maxim", "nok55@", "tas123", "38986765", "DevEducation");
        Connection connection = Mockito.mock(Connection.class);
        PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
        ResultSet resultSet = Mockito.mock(ResultSet.class);

        Mockito.when(databaseConnectionConfig.getConnection()).thenReturn(connection);
        Mockito.when(connection.prepareStatement(INSERT_REGISTRATION_DATA)).thenReturn(preparedStatement);
        Mockito.doNothing().when(preparedStatement).setString(1, user.getPassword());
        Mockito.doNothing().when(preparedStatement).setString(2, user.getEmail());
        Mockito.doNothing().when(preparedStatement).setString(3, user.getLogin());
        Mockito.doNothing().when(preparedStatement).setString(4, user.getCompany());
        Mockito.doNothing().when(preparedStatement).setString(5, user.getPhoneNumber());
        Mockito.when(preparedStatement.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(preparedStatement).close();
        Assertions.assertDoesNotThrow(() -> cut.registerUser(user));
    }

    @Test
    void checkIfExistTestReturnsTrue() throws SQLException {
        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Connection connection = Mockito.mock(Connection.class);
        Statement statement = Mockito.mock(Statement.class);
        boolean expected = true;
        Mockito.when(databaseConnectionConfig.getConnection()).thenReturn(connection);
        Mockito.when(connection.createStatement()).thenReturn(statement);
        Mockito.when(statement.executeQuery(CHECK_IF_USERS_AUTH_INFO_EXISTS)).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(LOGIN)).thenReturn("wrong login").thenReturn("correct login");
        Mockito.when(resultSet.getString(EMAIL)).thenReturn("wrong email").thenReturn("correct email");
        boolean actual = cut.checkIfExist("correct login", "correct email");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    void checkIfExistTestReturnsFalse() throws SQLException {
        ResultSet resultSet = Mockito.mock(ResultSet.class);
        Connection connection = Mockito.mock(Connection.class);
        Statement statement = Mockito.mock(Statement.class);
        boolean expected = false;
        Mockito.when(databaseConnectionConfig.getConnection()).thenReturn(connection);
        Mockito.when(connection.createStatement()).thenReturn(statement);
        Mockito.when(statement.executeQuery(CHECK_IF_USERS_AUTH_INFO_EXISTS)).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(LOGIN)).thenReturn("login1").thenReturn("login2");
        Mockito.when(resultSet.getString(EMAIL)).thenReturn("email1").thenReturn("email2");
        boolean actual = cut.checkIfExist("login3", "email3");
        Assertions.assertEquals(expected, actual);
    }
}
