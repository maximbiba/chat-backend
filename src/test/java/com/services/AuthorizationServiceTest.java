package com.services;

import com.dao.queries.AuthorizeQuery;
import com.dto.UserAuthDto;
import com.dto.ValidationDto;
import com.exception.BadRequestException;
import com.util.PasswordHashHelper;
import com.validators.UserAuthValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class AuthorizationServiceTest {

    private final AuthorizeQuery authorizeQuery = Mockito.mock(AuthorizeQuery.class);
    private final PasswordHashHelper passwordHashHelper = Mockito.mock(PasswordHashHelper.class);
    private final UserAuthValidator userAuthValidator = Mockito.mock(UserAuthValidator.class);
    private final AuthorizationService cut = new AuthorizationService(authorizeQuery, passwordHashHelper, userAuthValidator);

    @Test
    void authUserTest() {
        UserAuthDto userAuthDto = new UserAuthDto("koshka", "pomidoR123");
        ValidationDto validationDto = new ValidationDto(null, true);
        String expected = "Basic token";

        Mockito.when(userAuthValidator.validateUserAuth(userAuthDto)).thenReturn(validationDto);
        Mockito.when(passwordHashHelper.hashPassword(userAuthDto.getPassword())).thenReturn(userAuthDto.getPassword());

        String actual = cut.authUser(userAuthDto);

        Assertions.assertEquals(expected, actual);
        Mockito.verify(authorizeQuery, Mockito.times(1)).authorizeUser(userAuthDto.getLoginOrEmail(), userAuthDto.getPassword());
    }


    @Test
    void authUserBadRequestTest() {
        UserAuthDto userAuthDto = new UserAuthDto("koshka", "pomidoR123");
        ValidationDto validationDto = new ValidationDto("Ktos des potiryavsya", false);
        String expected = validationDto.getMessage();

        Mockito.when(userAuthValidator.validateUserAuth(userAuthDto)).thenReturn(validationDto);
        Mockito.when(passwordHashHelper.hashPassword(userAuthDto.getPassword())).thenReturn(userAuthDto.getPassword());

        BadRequestException actualException = Assertions.assertThrows(BadRequestException.class, () -> cut.authUser(userAuthDto));

        Assertions.assertEquals(expected, actualException.getMessage());
        Mockito.verify(authorizeQuery, Mockito.never()).authorizeUser(Mockito.any(), Mockito.any());
    }
}
