package com.servlets;

import com.dto.UserAuthDto;
import com.exception.GlobalException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.AuthorizationService;
import org.eclipse.jetty.http.HttpHeader;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Stream;
import static com.constants.Servlets.*;

class AuthServletTest {

    private final AuthorizationService authorizationService = Mockito.mock(AuthorizationService.class);
    private final ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);
    private final AuthServlet cut = new AuthServlet(objectMapper, authorizationService);

    @Test
    void doPostTest() throws IOException {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        PrintWriter printWriter = Mockito.mock(PrintWriter.class);
        UserAuthDto userAuthDto = new UserAuthDto("login", "password");

        Mockito.when(req.getReader()).thenReturn(bufferedReader);
        Mockito.when(bufferedReader.lines()).thenReturn(Stream.of("koshka"));
        Mockito.when(objectMapper.readValue("koshka", UserAuthDto.class)).thenReturn(userAuthDto);
        Mockito.when(authorizationService.authUser(userAuthDto)).thenReturn("someJwt");
        Mockito.when(resp.getWriter()).thenReturn(printWriter);

        cut.doPost(req, resp);

        Mockito.verify(resp, Mockito.times(1)).setHeader(HttpHeader.CONTENT_TYPE.toString(), TEXT_PLAIN);
        Mockito.verify(resp, Mockito.times(1)).setCharacterEncoding(CODER);
        Mockito.verify(authorizationService, Mockito.times(1)).authUser(userAuthDto);
        Mockito.verify(printWriter, Mockito.times(1)).write("someJwt");
        Mockito.verify(resp, Mockito.times(1)).setStatus(HttpServletResponse.SC_OK);
    }

    @Test
    void doPostIoExceptionTest() throws IOException {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);
        PrintWriter printWriter = Mockito.mock(PrintWriter.class);
        UserAuthDto userAuthDto = new UserAuthDto("login", "password");

        Mockito.when(req.getReader()).thenThrow(IOException.class);
        Mockito.when(resp.getWriter()).thenReturn(printWriter);

        cut.doPost(req, resp);

        Mockito.verify(resp, Mockito.times(1)).setHeader(HttpHeader.CONTENT_TYPE.toString(), TEXT_PLAIN);
        Mockito.verify(resp, Mockito.times(1)).setCharacterEncoding(CODER);
        Mockito.verify(authorizationService, Mockito.never()).authUser(Mockito.any());
        Mockito.verify(printWriter, Mockito.times(1)).write(WRONG_PARS_DATA);
        Mockito.verify(resp, Mockito.times(1)).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Test
    void doPostGlobalExceptionExceptionTest() throws IOException {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        PrintWriter printWriter = Mockito.mock(PrintWriter.class);
        UserAuthDto userAuthDto = new UserAuthDto("login", "password");

        Mockito.when(req.getReader()).thenReturn(bufferedReader);
        Mockito.when(bufferedReader.lines()).thenReturn(Stream.of("koshka"));
        Mockito.when(objectMapper.readValue("koshka", UserAuthDto.class)).thenReturn(userAuthDto);
        Mockito.when(authorizationService.authUser(userAuthDto)).thenThrow(new GlobalException("pomidor", 400));
        Mockito.when(resp.getWriter()).thenReturn(printWriter);

        cut.doPost(req, resp);

        Mockito.verify(resp, Mockito.times(1)).setHeader(HttpHeader.CONTENT_TYPE.toString(), TEXT_PLAIN);
        Mockito.verify(resp, Mockito.times(1)).setCharacterEncoding(CODER);
        Mockito.verify(authorizationService, Mockito.times(1)).authUser(userAuthDto);
        Mockito.verify(printWriter, Mockito.times(1)).write("pomidor");
        Mockito.verify(resp, Mockito.times(1)).setStatus(400);
    }
}
