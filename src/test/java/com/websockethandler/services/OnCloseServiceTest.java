package com.websockethandler.services;

import com.cache.UserCache;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static com.constants.Logger.USER_DISCONNECTED;

class OnCloseServiceTest {

    @Test
    void onCloseTest() throws IOException {

        Session session = Mockito.mock(Session.class);
        RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
        ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);
        UserCache userCache = new UserCache();
        userCache.addUser(session);
        OnCloseService onCloseService = new OnCloseService(objectMapper, userCache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        Mockito.doNothing().when(remoteEndpoint).sendString(USER_DISCONNECTED);
        Assertions.assertDoesNotThrow(() -> onCloseService.onClose(session));
        Mockito.verify(session, Mockito.times(0)).getRemote();
    }

}
