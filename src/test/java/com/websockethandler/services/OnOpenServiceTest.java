package com.websockethandler.services;

import com.cache.UserCache;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static com.constants.Logger.USER_CONNECTED;

class OnOpenServiceTest {
    @Test
    void onOpenTest() throws IOException {

        Session session = Mockito.mock(Session.class);
        RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
        ObjectMapper objectMapper = Mockito.mock(ObjectMapper.class);
        UserCache userCache = new UserCache();
        userCache.addUser(session);
        OnOpenService onOpenService = new OnOpenService(objectMapper, userCache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        Mockito.doNothing().when(remoteEndpoint).sendString(USER_CONNECTED);
        Assertions.assertDoesNotThrow(() -> onOpenService.onOpen(session));
        Mockito.verify(session, Mockito.times(0)).getRemote();
    }

}
