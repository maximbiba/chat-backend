package com.websockethandler.services;

import com.cache.UserCache;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static com.constants.Logger.MSG;

public class OnTextServiceTest {
    @Test
    void onTextTest() throws IOException {

        Session session = Mockito.mock(Session.class);
        RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
        UserCache userCache = new UserCache();
        userCache.addUser(session);
        OnTextService onTextService = new OnTextService(userCache);
        String message = "123";
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        Mockito.doNothing().when(remoteEndpoint).sendString(MSG + message);
        Assertions.assertDoesNotThrow(() -> onTextService.onText(session, message));
        Mockito.verify(session, Mockito.times(0)).getRemote();
    }
    @Test
    void onTextTestTwoUsers() throws IOException {

        Session mysession = Mockito.mock(Session.class);
        Session othersession = Mockito.mock(Session.class);
        RemoteEndpoint myRemoteEndpoint = Mockito.mock(RemoteEndpoint.class);
        RemoteEndpoint otherRemoteEndpoint = Mockito.mock(RemoteEndpoint.class);

        UserCache userCache = new UserCache();
        userCache.addUser(mysession);
        userCache.addUser(othersession);
        OnTextService onTextService = new OnTextService(userCache);
        String message = "123";

        Mockito.when(mysession.getRemote()).thenReturn(myRemoteEndpoint);
        Mockito.doNothing().when(myRemoteEndpoint).sendString(MSG + message);

        Mockito.when(othersession.getRemote()).thenReturn(otherRemoteEndpoint);
        Mockito.doNothing().when(otherRemoteEndpoint).sendString(MSG + message);

        Assertions.assertDoesNotThrow(() -> onTextService.onText(mysession, message));

        Mockito.verify(mysession, Mockito.times(0)).getRemote();
        Mockito.verify(othersession, Mockito.times(1)).getRemote();
        Mockito.verify(myRemoteEndpoint, Mockito.times(0)).sendString(MSG + message);
        Mockito.verify(otherRemoteEndpoint, Mockito.times(1)).sendString(MSG + message);
    }
}
